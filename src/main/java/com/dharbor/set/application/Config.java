package com.dharbor.set.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ivan Alban
 */
@Configuration
@ComponentScan("com.dharbor.set.application")
public class Config {
}
