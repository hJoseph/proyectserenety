package com.dharbor.set.application.components.sentMail;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class OpenSentSection implements Task {

    @Autowired
    private SentComponent sentComponent;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                sentComponent.pushInboxButton()
        );
    }
}
