package com.dharbor.set.application.components.draftMail;

/**
 * @author Henry J. Calani A.
 */
public class ConstantsDraft {

    public static final String TITLE_DRAFT_HEADER = ".showcase-panel-header";
    public static final String ICON_DRAFT = "//ul/li[3]";
    public static final String DRAFT_TITLE =  "Form title";
    public static final String DRAFT_TITLE_LI =  "Form title";
    public static final String DRAFT_TITLE_MESSAGING =  "Form title";

}
