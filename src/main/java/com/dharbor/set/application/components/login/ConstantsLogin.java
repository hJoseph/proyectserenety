package com.dharbor.set.application.components.login;


/**
 * @author Henry J. Calani A.
 */
public class ConstantsLogin {

    public static final String TITLE_DRAFT_HEADER = ".showcase-panel-header";
    public static final String ICON_DRAFT = "//ul/li[3]";
}
