package com.dharbor.set.application.components.draftMail;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

/**
 * @author Henry J. Calani A.
 */
public class OpenDraftSection implements Task {

    @Autowired
    private DraftComponent draftComponent;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                draftComponent.pushDraftButton(),
                WaitUntil.the(draftComponent.getDraftTitle(),isVisible()).forNoMoreThan(50).seconds()

        );
    }
}
