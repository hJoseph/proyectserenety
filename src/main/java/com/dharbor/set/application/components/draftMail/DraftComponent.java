package com.dharbor.set.application.components.draftMail;

import com.dharbor.set.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import javax.annotation.PostConstruct;

/**
 * @author Henry J. Calani A.
 */
@PrototypeScope
class DraftComponent {

    @Getter
    private Target draftTitle;

    @Getter
    private Target headerTitle;

    @Getter
    private Target clickDraft;

    Performable pushDraftButton() {
        return Click.on(clickDraft);
    }

    @PostConstruct
    void onPostConstruct() {
        headerTitle = Target.the(ConstantsDraft.DRAFT_TITLE_MESSAGING).located(By.className(ConstantsDraft.TITLE_DRAFT_HEADER));
        draftTitle = Target.the(ConstantsDraft.DRAFT_TITLE).located(By.xpath(ConstantsDraft.ICON_DRAFT));
        clickDraft = Target.the(ConstantsDraft.DRAFT_TITLE_LI).located(By.xpath(ConstantsDraft.ICON_DRAFT));

    }
}
