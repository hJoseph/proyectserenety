package com.dharbor.set.application.components.sentMail;

/**
 * @author Henry J. Calani A.
 */
public class ConstantsSentMail {

    public static final String HEADER_LOGIN = ".showcase-panel-header";
    public static final String LABEL_SEND_MAIL = "Sent title send Mail";
    public static final String ICON_SEND_MAIL = "//li[@class='smg-txt smg-txt-large smg-txt-ellipsis smg-folder-option-collapse-text ng-star-inserted']";
    public static final String TITLE_SEND_MAIL= "Form title";


}
