package com.dharbor.set.application.components.sentMail;

import com.dharbor.set.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import javax.annotation.PostConstruct;

/**
 * @author Henry J. Calani A.
 */
@PrototypeScope
class SentComponent {

    @Getter
    private Target inboxTitle;

    @Getter
    private Target headerTitle;

    @Getter
    private Target clickInbox;

    Performable pushInboxButton() {
        return Click.on(clickInbox);
    }

    @PostConstruct
    void onPostConstruct() {
        headerTitle = Target.the(ConstantsSentMail.TITLE_SEND_MAIL).located(By.className(ConstantsSentMail.HEADER_LOGIN));
        inboxTitle = Target.the(ConstantsSentMail.LABEL_SEND_MAIL).located(By.xpath(ConstantsSentMail.ICON_SEND_MAIL));
        clickInbox = Target.the(ConstantsSentMail.LABEL_SEND_MAIL).located(By.xpath(ConstantsSentMail.ICON_SEND_MAIL));

    }
}
