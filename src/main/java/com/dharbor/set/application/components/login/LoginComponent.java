package com.dharbor.set.application.components.login;

import com.dharbor.set.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.annotation.PostConstruct;

/**
 * @author Ivan Alban
 */
@PrototypeScope
class LoginComponent {

    @Getter
    private Target headerTitle;

    @Getter
    private Target emailLabel;

    @Getter
    private Target emailInput;

    @Getter
    private Target passwordLabel;

    @Getter
    private Target passwordInput;

    @Getter
    private Target loginButton;

    Performable enterEmailValue(String value) {
        return Enter.theValue(value).into(emailInput).thenHit(Keys.TAB);
    }

    Performable enterPasswordValue(String value) {
        return Enter.theValue(value).into(passwordInput).thenHit(Keys.TAB);
    }

    Performable pushLoginButton() {
        return Click.on(loginButton);
    }

    @PostConstruct
    void onPostConstruct() {

        headerTitle = Target.the("Form title").located(By.className("public-login-form-header"));
        emailLabel = Target.the("Email label").located(By.className("public-login-form-header"));
        emailInput = Target.the("Email input").located(By.id("username"));
        passwordLabel = Target.the("Password label").located(By.className("public-login-form-header"));
        passwordInput = Target.the("Password input").located(By.id("password"));
        loginButton = Target.the("Login button").located(By.xpath(".//button[@type='submit']"));

    }
}
