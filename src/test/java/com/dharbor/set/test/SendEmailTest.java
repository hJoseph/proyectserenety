package com.dharbor.set.test;

import com.dharbor.set.application.components.browser.OpenBrowser;
import com.dharbor.set.application.components.sentMail.OpenSentSection;
import com.dharbor.set.application.components.login.InsertCredentialsAndLogin;
import com.dharbor.set.application.config.ApplicationConfig;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.GivenWhenThen.*;

/**
 * @author Henry J. Calani A.
 */
public class SendEmailTest extends AbstractTest {

    @Autowired
    private ApplicationConfig config;

    private Actor actor = Actor.named("henry");

    private OpenBrowser openBrowser;

    private InsertCredentialsAndLogin insertCredentialsAndLogin;

    private OpenSentSection openSendInbox;



    @Before
    public void setup() {
        initializeActor(actor);

        openBrowser = taskInstance(OpenBrowser.class);

        insertCredentialsAndLogin = taskInstance(InsertCredentialsAndLogin.class);
        insertCredentialsAndLogin.setEmail(config.getUser());
        insertCredentialsAndLogin.setPassword(config.getPassword());

        openSendInbox = taskInstance(OpenSentSection.class);
    }

    @WithTag("openSendMail")
    @Test
    public void userLogin() {
        givenThat(actor)
                .attemptsTo(openBrowser);
        when(actor)
                .attemptsTo(insertCredentialsAndLogin);
        then(actor)
                .attemptsTo(openSendInbox);
    }
}
