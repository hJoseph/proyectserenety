package com.dharbor.set.test;

import com.dharbor.set.application.components.browser.OpenBrowser;
import com.dharbor.set.application.components.login.*;
import com.dharbor.set.application.config.ApplicationConfig;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;

/**
 * @author Henry J. Calani A.
 */
public class LoginApplicationTest extends AbstractTest {

    @Autowired
    private ApplicationConfig config;

    private Actor actor = Actor.named("henry");

    private OpenBrowser openBrowser;

    private InsertCredentialsAndLogin insertCredentialsAndLogin;

    @Before
    public void setup() {
        initializeActor(actor);

        openBrowser = taskInstance(OpenBrowser.class);

        insertCredentialsAndLogin = taskInstance(InsertCredentialsAndLogin.class);
        insertCredentialsAndLogin.setEmail(config.getUser());
        insertCredentialsAndLogin.setPassword(config.getPassword());
    }

    @WithTag("loginEnvironmetSet")
    @Test
    public void userLogin() {
        givenThat(actor)
                .attemptsTo(openBrowser);

        then(actor)
                .attemptsTo(insertCredentialsAndLogin);
    }
}
