package com.dharbor.set.test;

import com.dharbor.set.application.components.browser.OpenBrowser;
import com.dharbor.set.application.components.login.*;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.is;

/**
 * @author Ivan Alban
 */
public class OpenApplicationTest extends AbstractTest {

    private Actor actor = Actor.named("ivan");

    private OpenBrowser openBrowser;

    private TheHeaderTitle theHeaderTitle;

    private TheLoginButtonExists theLoginButtonExists;

    private TheEmailInputExists theEmailInputExists;

    private ThePasswordInputExists thePasswordInputExists;

    @Before
    public void setup() {
        initializeActor(actor);

        openBrowser = taskInstance(OpenBrowser.class);

        theHeaderTitle = questionInstance(TheHeaderTitle.class);

        theLoginButtonExists = questionInstance(TheLoginButtonExists.class);

        theEmailInputExists = questionInstance(TheEmailInputExists.class);

        thePasswordInputExists = questionInstance(ThePasswordInputExists.class);
    }

    @WithTag("openAplication")
    @Test
    public void userOpenApplication() {
        givenThat(actor)
                .attemptsTo(openBrowser);

        then(actor).should(
                seeThat(theHeaderTitle, is("Please Login")),
                seeThat(theEmailInputExists),
                seeThat(thePasswordInputExists),
                seeThat(theLoginButtonExists)
        );
    }
}
